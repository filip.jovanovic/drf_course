from django.contrib import admin

from watchlist_app.models import WatchList, StreamPlatform, Review

# Register your models here.
# john, john7773
# filipj, password

admin.site.register(WatchList)
admin.site.register(StreamPlatform)
admin.site.register(Review)